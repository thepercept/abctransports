# My project's README.

Note : Please replace the folder path according to your machine's project structure.

---- Setting it Up------

A.) Install all dependies :

  Create a virtual environment:

  virtualenv venv -p python3.5

B.) Activate virtual environment and install the mentioned libraries  
  source bin/activate

  pip install django==1.11

  pip install celery

  pip install redis

  pip install django-celery-beat

  pip install django-celery-results




C.) Perform these :
 python manage.py makemigrations manager

 python manage.py makemigrations transporter

 python manage.py migrate

 python manage.py createsuperuser

 Note : give a username and password (required)

 python manage.py runserver





D.) Open a new Terminal and create environment variable and start celery
  1.)
  export PYTHONPATH=/home/thebitshoes/Desktop/DjangoEnvironment/abctransports:$PYTHONPATH

  2.)
  /home/thebitshoes/Desktop/DjangoEnvironment/bin/celery --app=abctransports.celery:app worker --loglevel=INFO


E.) Again open a new Terminal and create environment variable and start celery beat

  1.)
  export PYTHONPATH=/home/thebitshoes/Desktop/DjangoEnvironment/abctransports:$PYTHONPATH

  2.)
  Now 'cd' to your virtual environment and activate it

  3.)
  celery -A abctransports beat -l info


For Manager:



# Update :
Under manager.utility.py -->

  -- Uncomment line 402 and comment out line 398 to see the notifications today.
  -- Or else the notifications are shown at the exact dates


As a manager (admin) of the ABC Transports, one has to do the following tasks:
Open HomePage --> http://127.0.0.1:8000/

1. Add new trucks
    -- Enter Truck Details
    -- Enter Insurance Details
    -- Enter Fitness Details
    -- Enter Pollution Certificate

    * All fields are mandatory
  -- SAVE

2. Add Transporters(a.k.a Drivers)
    -- Enter Transporter Details

    * All fields are mandatory
    -- SAVE


3. Assign Transporters to a Truck so that ABC Transport knows who's driving what truck
    -- Assign Driver from HomePage



For Transporter

As a Transporter, the person should have an account on the system. The account is created when the driver is assigned to a truck, by the manager(admin) and a Transporter ID is generated.


You can get the Transporter ID form the table in  Home > Transporters

The Transporter ID and Password are same for the time being. You should use this ID to login to the Transporter's  dashboard.

Open Transporter Page --> http://127.0.0.1:8000/transporter/

  -- Now you can chek notifications here.
