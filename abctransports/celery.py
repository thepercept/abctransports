from __future__ import absolute_import,unicode_literals
import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'abctransports.settings')

app = Celery('abctransports')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings',namespace='CELERY')
#app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.autodiscover_tasks()



app.conf.beat_schedule = {
    'test-every-5-seconds': {
        'task': 'test_task_one',
        'schedule': 5.0,
        'args': ()
    },

}
