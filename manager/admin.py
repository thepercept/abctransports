from django.contrib import admin
from manager.models import Truck,InsuranceDetails,FitnessDetails,PollutionCertificate


# Register your models here.
admin.site.register(Truck)
admin.site.register(InsuranceDetails)
admin.site.register(FitnessDetails)
admin.site.register(PollutionCertificate)
