from django.contrib.auth.models import User
from manager.models import Truck,InsuranceDetails,FitnessDetails,PollutionCertificate
from django import forms

from django.core.exceptions import ValidationError
import datetime


class LoginForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Password'}))

    class Meta:
        model = User
        fields = ['username','password']

class TruckForm(forms.Form):
    brand_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Brand Name'}))
    model_no = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Truck Model'}))
    reg_id = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'eg: DL 2C 6011','min':'6','max':'8'}))

    def clean_reg_id   (self):
        registration_id = self.cleaned_data['reg_id']
        if len(registration_id) < 6:
            raise ValidationError(('Registration no should be atleast 6 letters long'))
        return registration_id




class InsuranceDetailsForm(forms.Form):

    insurance_doc = forms.FileField(widget=forms.ClearableFileInput(attrs={'class':'file-input'}))
    ins_from_date = forms.DateField(widget=forms.DateInput(attrs={'type':'text','class':'form-control', 'placeholder':'yyyy-mm-dd'}))
    ins_to_date = forms.DateField(widget=forms.DateInput(attrs={'type':'text','class':'form-control', 'placeholder':'yyyy-mm-dd'}))

    def clean_ins_from_date   (self):
        insurance_start_date = self.cleaned_data['ins_from_date']
        print("Checking date..")
        if insurance_start_date > datetime.date.today():
            raise ValidationError(('Invalid insurance start date'))
        return insurance_start_date

    def clean_ins_to_date   (self):
        insurance_expiry_date = self.cleaned_data['ins_to_date']
        print("Checking date..")
        if insurance_expiry_date < datetime.date.today():
            raise ValidationError(('Invalid date - Insurance cannot be in past'))
        return insurance_expiry_date



class FitnessDetailsForm(forms.Form):
    fitness_doc = forms.FileField(widget=forms.ClearableFileInput(attrs={'class':'file-input'}))
    fit_from_date = forms.DateField(widget=forms.DateInput(attrs={'type':'text','class':'form-control', 'placeholder':'yyyy-mm-dd'}))
    fit_to_date = forms.DateField(widget=forms.DateInput(attrs={'type':'text','class':'form-control', 'placeholder':'yyyy-mm-dd'}))

class PollutionCertificateForm(forms.Form):
    pollution_doc = forms.FileField(widget=forms.ClearableFileInput(attrs={'class':'file-input'}))
    pol_f_date = forms.DateField(widget=forms.DateInput(attrs={'type':'text','class':'form-control', 'placeholder':'yyyy-mm-dd'}))
    pol_to_date = forms.DateField(widget=forms.DateInput(attrs={'type':'text','class':'form-control', 'placeholder':'yyyy-mm-dd'}))
