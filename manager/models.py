from django.db import models
from django.utils import timezone


# Create your models here.
class Truck(models.Model):
    AVAILABILITY_CHOICES = (
        ('yes','Available'),
        ('no','Not Available'),
    )

    brand_name = models.CharField (max_length=40, blank=False, null=False)
    model_no = models.CharField (max_length=20, blank=False, null=False)
    reg_id = models.CharField (max_length=20, blank=False, null=False)
    available = models.CharField(choices=AVAILABILITY_CHOICES,max_length=3, null=False,default="yes")


    def __str__(self):
        return "(%s-%s) : %s" % (self.brand_name,self.model_no,self.reg_id)

class InsuranceDetails(models.Model):
    truck = models.OneToOneField(Truck,null=False)
    insurance_doc = models.FileField(upload_to = 'insurance_docs/')
    ins_from_date = models.DateField()
    ins_to_date = models.DateField()

    def __str__(self):
        return "(%s-%s) :" % (self.truck,self.insurance_doc)

class FitnessDetails(models.Model):
    truck = models.OneToOneField(Truck,null=False)
    fitness_doc = models.FileField(upload_to = 'fitness_docs/')
    fit_from_date = models.DateField()
    fit_to_date = models.DateField()

    def __str__(self):
        return "(%s-%s)" % (self.fitness_doc,self.truck)

class PollutionCertificate(models.Model):
    truck = models.OneToOneField(Truck,null=False)
    pollution_doc = models.FileField(upload_to = 'pollution_docs/')
    pol_from_date = models.DateField()
    pol_to_date = models.DateField()


    def __str__(self):
        return "(%s-%s)" % (self.pollution_doc,self.truck)
