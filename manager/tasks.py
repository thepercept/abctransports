from __future__ import absolute_import
#from celery import shared_task
from celery.decorators import task
from manager.models import Truck,InsuranceDetails,FitnessDetails,PollutionCertificate
#from . utiltiy import checkReminders
from transporter.models import Reminder
from manager.utility import checkReminders,updateReminder

def utility():
    #obj = Truck.objects.all()
    count  = 0

    reminders_obj = Reminder.objects.all()
    for x in reminders_obj:
        count+=1
    return count




@task(name="test_task_one")
def test():
    message ="No reminders for today" 

    if len(checkReminders()) > 0:
        message = updateReminder()

    return message
