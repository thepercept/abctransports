from django.conf.urls import url
from manager.views import index,index_login,trucks,addtruck,viewtruck,assign,addtransporter,alltransporters,alltrucks

urlpatterns = [
    url(r'^login/',index_login,name='index_login'),
    url(r'^$',index,name='index'),
    url(r'^trucks/',trucks,name='trucks'),
    url(r'^addtruck/',addtruck,name='addtruck'),
    url(r'^driver/(?P<reg_id>\w+)/$',viewtruck,name='driver'),
    url(r'^driver/(?P<reg_id>\w+)/assign/(?P<transporter_id>\w+)$',assign,name='assign'),
    url(r'^addtransporter/',addtransporter,name='addtransporter'),
    url(r'^alltransporters/',alltransporters,name='alltransporters'),
    url(r'^alltrucks/',alltrucks,name='alltrucks'),



]
