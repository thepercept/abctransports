from manager.models import Truck,InsuranceDetails,FitnessDetails,PollutionCertificate
from transporter.models import TransporterDetails,Reminder
from datetime import datetime, timedelta
import datetime


import json


def getTruck(reg_id):
    truck_instance = None
    try:
        truck_instance = Truck.objects.get(reg_id=reg_id)
    except Exception as e:
        raise
    print("GETTING TRUCK BY REG ID")
    return truck_instance

def getInsurance(reg_id):
    ins_instance = None
    try:
        ins_instance = InsuranceDetails.objects.get(truck=getTruck(reg_id))
    except Exception as e:
        raise
    return ins_instance

def getFitness(reg_id):
    fit_instance = None
    try:
        fit_instance = FitnessDetails.objects.get(truck=getTruck(reg_id))
    except Exception as e:
        raise
    return fit_instance

def getInsuranceStartDate(reg_id):
    ins_instance = getInsurance(reg_id)
    start_date = str(ins_instance.ins_from_date)
    return start_date


def getInsuranceExpiryDate(reg_id):
    ins_instance = getInsurance(reg_id)
    expiry_date = str(ins_instance.ins_to_date)
    return expiry_date

def getFitnessStartDate(reg_id):
    fit_instance = getFitness(reg_id)
    start_date = str(fit_instance.fit_from_date)
    return start_date


def getFitnessExpiryDate(reg_id):
    fit_instance = getFitness(reg_id)
    expiry_date = str(fit_instance.fit_to_date)
    return expiry_date

def convertListToJson(array_list):
	json_string = json.dumps(array_list)
	return json_string

def getTruckData(reg_id):
    truck_data = []
    truck_data_list = []

    truck_instance = getTruck(reg_id)
    ins_instance = getInsurance(reg_id)
    fit_instance = getFitness(reg_id)


    truck_data.append(truck_instance.reg_id)
    truck_data.append(truck_instance.model_no)
    truck_data.append(truck_instance.brand_name)
    truck_data.append(getInsuranceStartDate(reg_id))
    truck_data.append(getInsuranceExpiryDate(reg_id))
    truck_data.append(getFitnessStartDate(reg_id))
    truck_data.append(getFitnessExpiryDate(reg_id))



    truck_data.append("STATUS")

    truck_data_list.append(truck_data)


    truck_data_dictionary = convertListToJson(truck_data_list)

    print("____________________________________________",truck_data_dictionary)
    return truck_data_dictionary









def getFitnessNotificationTimings(reg_id):
    N30 = None
    N15 = None
    N7 = None

    fit_instance = getFitness(reg_id)
    expiry_date = fit_instance.fit_to_date


    remaining_days =fit_instance.fit_to_date - datetime.date.today()

    delta30 = timedelta(days=30)
    delta15 = timedelta(days=15)
    delta7 = timedelta(days=7)

    print(str(remaining_days))
    if remaining_days >= delta30 :
        N30 = fit_instance.fit_to_date - delta30
        N15 = fit_instance.fit_to_date - delta15
        N7 = fit_instance.fit_to_date - delta7
        print ("More that 30 days.Creating 3 notifications N30,N15,N7",N30,N15,N7)
        return N30
    elif remaining_days >= delta15 and remaining_days < delta30 :
        print("Date is between 15 and 30")
        N15 = fit_instance.fit_to_date - delta15
        return N15
    elif remaining_days >= delta7 and remaining_days < delta15:
        N7 = fit_instance.fit_to_date - delta7
        return N7
    else:
        return None


def thirty_and_above(reg_id,check,expiry_date):
    delta30 = timedelta(days=30)
    delta15 = timedelta(days=15)
    delta7 = timedelta(days=7)
    instance = None
    if check =='fitness':
        instance = getFitness(reg_id)
        N30 = instance.fit_to_date - delta30
        N15 = instance.fit_to_date - delta15
        N7 = instance.fit_to_date - delta7
    else:
        instance = getInsurance(reg_id)
        N30 = instance.ins_to_date - delta30
        N15 = instance.ins_to_date - delta15
        N7 = instance.ins_to_date - delta7
    notifications = []
    notifications.append(N30)
    notifications.append(N15)
    notifications.append(N7)
    return notifications


def fifteen_and_thirty(reg_id,check,expiry_date):
    delta15 = timedelta(days=15)
    delta7 = timedelta(days=7)
    instance = None

    if check =='fitness':
        instance = getFitness(reg_id)
        N15 = instance.fit_to_date - delta15
        N7 = instance.fit_to_date - delta7
    else:
        instance = getInsurance(reg_id)
        N15 = instance.ins_to_date - delta15
        N7 = instance.ins_to_date - delta7
    notifications = []
    notifications.append(N15)
    notifications.append(N7)
    return notifications



def seven_and_fifteen(reg_id,check,expiry_date):
    delta7 = timedelta(days=7)
    instance = None

    if check =='fitness':
        instance = getFitness(reg_id)
        N7 = instance.fit_to_date - delta7
    else:
        instance = getInsurance(reg_id)
        N7 = instance.ins_to_date - delta7
    return N7







def getTrucksData():

    all_trucks = Truck.objects.filter(available = "yes")
    if len(all_trucks)>0:
        truck_data = []
        truck_data_list = []
        for truck in all_trucks:
            reg_id = truck.reg_id

            print("_________CHECKING ONLY ________")
            truck_instance=getTruck(reg_id)

            truck_data.append(truck.reg_id)
            truck_data.append(truck.brand_name)
            truck_data.append(truck.model_no)

            # add insurance details
            truck_data.append(getInsuranceExpiryDate(reg_id))
            # add Fitness details
            truck_data.append(getFitnessExpiryDate(reg_id))

            truck_data.append("ASIGN DRIVER")
            truck_data_list.append(truck_data)
            truck_data = []

        truck_data_dict = convertListToJson(truck_data_list)

        print("TABLE DATA",truck_data_dict)
        return truck_data_dict
    else:
        print("No Truck data exist")


def getTransporter(transporter_id):
    transporter_instance = None
    try:
        transporter_instance = TransporterDetails.objects.get(transporter_id=transporter_id)
    except Exception as e:
        raise
    return transporter_instance








def getAvailableTransporters():

    all_transporters = None

    try:
        all_transporters = TransporterDetails.objects.filter(available="yes")
        print(len(all_transporters))
    except Exception as e:
        raise


    if len(all_transporters)>0:
        transporter_data = []
        transporter_data_list = []

        for transporter in all_transporters:
            transporter_data.append(transporter.transporter_id)
            transporter_data.append(transporter.transporter_name)
            transporter_data.append(transporter.transporter_age)
            if transporter.available == 'yes':
                transporter_data.append("ASSIGN")
            else:
                transporter_data.append("BUSY")

            transporter_data_list.append(transporter_data)
            transporter_data = []

        transporter_data_dict = convertListToJson(transporter_data_list)
        print("TRANSPORTER DATA .....PRINTING",transporter_data_dict)
        return transporter_data_dict
    else:
        all_transporters = [[]]
        print(all_transporters)
        print("No drivers exist")


def getAllTransporters():
    all_transporters = None

    try:
        all_transporters = TransporterDetails.objects.all()
    except Exception as e:
        raise


    if len(all_transporters)>0:
        transporter_data = []
        transporter_data_list = []

        for transporter in all_transporters:
            transporter_data.append(transporter.transporter_id)
            transporter_data.append(transporter.transporter_name)
            transporter_data.append(transporter.transporter_age)
            if transporter.available == 'yes':
                transporter_data.append("Not Assigned")
            else:
                transporter_data.append("Assigned")

            transporter_data_list.append(transporter_data)
            transporter_data = []

        transporter_data_dict = convertListToJson(transporter_data_list)
        return transporter_data_dict
    else:
        all_transporters = [[]]



def assignTransporter(reg_id,transporter_id):
    transporter_instance = getTransporter(transporter_id)

    if transporter_instance is not None and transporter_instance.available == "yes":
        if TransporterDetails.objects.filter(transporter_id=transporter_id).update(truck=getTruck(reg_id),available="no"):
            print("aSSIGNING Transporter",reg_id)
            Truck.objects.filter(reg_id=reg_id).update(available="no")





            # Get Dates of Insurance and Fitness
            ins_instance = getInsurance(reg_id)
            ins_expiry_date = ins_instance.ins_to_date

            fit_instance = getFitness(reg_id)
            fit_expiry_date = fit_instance.fit_to_date

            # Check what category do the dates fall in . N30 or N15 or N7

            remaining_ins_days = ins_expiry_date - datetime.date.today()
            remaining_fit_days = fit_expiry_date - datetime.date.today()

            delta30 = timedelta(days=30)
            delta15 = timedelta(days=15)
            delta7 = timedelta(days=7)

            # ins reminders
            ins_note_30 = "Your vehicle will expire in 30 days."
            ins_note_15 = "Your vehicle will expire in 15 days."
            ins_note_7 = "Your vehicle   will expire in 7 days."


            if remaining_ins_days > delta30:
                three_notif = thirty_and_above(reg_id,"insurance",ins_expiry_date)
                for r_date in three_notif:
                    reminder_obj = Reminder(transporter=getTransporter(transporter_id),reminder_date=r_date,reminder_note=ins_note_30,reminder_status="active")
                    reminder_obj.save()

            elif remaining_ins_days >= delta15 and remaining_ins_days < delta30 :
                two_notif = fifteen_and_thirty(reg_id,"insurance",ins_expiry_date)
                for r_date in two_notif:
                    reminder_obj = Reminder(transporter=getTransporter(transporter_id),reminder_date=r_date,reminder_type=ins_note_15,reminder_status="active")
                    reminder_obj.save()

            elif remaining_ins_days >= delta7 and remaining_ins_days < delta15:
                one_notif_date = seven_and_fifteen(reg_id,"insurance",ins_expiry_date)
                reminder_obj = Reminder(transporter=getTransporter(transporter_id),reminder_date=one_notif_date,reminder_type=ins_note_7,reminder_status="active")
                reminder_obj.save()
            else:
                pass


            #  fit reminders
            fit_note_30 = "Your vehicle fitness will expire in 30 days."
            fit_note_15 = "Your vehicle fitness will expire in 15 days."
            fit_note_7 = "Your vehicle fitness will expire in 7 days."

            if remaining_fit_days > delta30:
                three_notif = thirty_and_above(reg_id,"fitness",fit_expiry_date)
                for r_date in three_notif:
                    reminder_obj = Reminder(transporter=getTransporter(transporter_id),reminder_date=r_date,reminder_note=fit_note_30,reminder_status="active")
                    reminder_obj.save()

            elif remaining_fit_days >= delta15 and remaining_fit_days < delta30 :
                two_notif = fifteen_and_thirty(reg_id,"fitness",fit_expiry_date)
                for r_date in two_notif:
                    reminder_obj = Reminder(transporter=getTransporter(transporter_id),reminder_date=r_date,reminder_type=fit_note_15,reminder_status="active")
                    reminder_obj.save()

            elif remaining_fit_days >= delta7 and remaining_fit_days < delta15:
                one_notif_date = seven_and_fifteen(reg_id,"fitness",fit_expiry_date)
                reminder_obj = Reminder(transporter=getTransporter(transporter_id),reminder_date=one_notif_date,reminder_type=fit_note_7,reminder_status="active")
                reminder_obj.save()
            else:
                pass
            print("ASSIGNED !!!!!")
        else:
            print("NOT ASSIGNED ... ERR")
    else:
        pass



def checkReminders():

    all_reminders = None

    try:
        # For notification at exact dates of delivery
        all_reminders = Reminder.objects.filter(reminder_status='active',reminder_date=datetime.datetime.today())


        # For testing notification
        #all_reminders = Reminder.objects.filter(reminder_status='active')


        print("The no of active reminders are ::",len(all_reminders))
    except Exception as e:
        raise

    return all_reminders


def updateReminder():
    #pass argument below : reminder_date=datetime.datetime.today()
    all_reminders = Reminder.objects.filter(reminder_status="active")


    if len(all_reminders)>0:
        for reminder in all_reminders:
            Reminder.objects.filter(transporter=reminder.transporter).update(reminder_status="sent")
        return "UPDATED to SENT"
