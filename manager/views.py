from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from abctransports import settings
from django.contrib.auth import authenticate,login,logout
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from . models import Truck,InsuranceDetails,FitnessDetails,PollutionCertificate
from transporter.models import TransporterDetails
from . forms import LoginForm,TruckForm,InsuranceDetailsForm,FitnessDetailsForm,PollutionCertificateForm
from transporter.forms import TransporterDetailsForm
import uuid
from . utility import getTruck,getTruckData,getTrucksData,getAvailableTransporters,assignTransporter,getAllTransporters,checkReminders

# Create your views here.

def index_login(request):
    next = request.GET.get('next','/')
    login_form = LoginForm()
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username,password=password)
        print("User >>>>",user)

        if user is not None:
            if user.is_active:
                login(request,user)
                return HttpResponseRedirect(next)
            else:
                HttpResponse("Inactive User")
        else :
            if request.user.is_authenticated():
                return HttpResponseRedirect('index')
                print("User is authenticated")
            else:
                return HttpResponseRedirect(settings.LOGIN_URL)
    return render(request, "manager/login.html", {'redirect_to':next,'login_form':login_form})


@login_required
def index(request):
    print("ON HOMEPAGE.......",checkReminders())

    truck_data_dictionary ={}

    if request.method=='POST':
        pass
    else:
        truck_data_dictionary = getTrucksData()




    return render(request,"manager/index.html",{'truck_data_dictionary':truck_data_dictionary,})

@login_required
def trucks(request):
    pass


@login_required
def addtruck(request):

    if request.method == 'POST':
        print("CHECK A.............")
        if 'SAVE_TRUCK' in request.POST:
            print("CHECK B.............")
            truck_details_form = TruckForm(request.POST)
            insurance_detail_form = InsuranceDetailsForm(request.POST,request.FILES)
            fitness_detail_form = FitnessDetailsForm(request.POST,request.FILES)
            pollution_certificate_form = PollutionCertificateForm(request.POST,request.FILES)

            if truck_details_form.is_valid() and insurance_detail_form.is_valid() and fitness_detail_form.is_valid() and pollution_certificate_form.is_valid() :
                print("CHECK --Form VALID.............")
                # Truck Details
                brand_name = truck_details_form.cleaned_data['brand_name']
                model_no = truck_details_form.cleaned_data['model_no']
                reg_id = truck_details_form.cleaned_data['reg_id']

                truck_obj=Truck(brand_name=brand_name,model_no=model_no,reg_id=reg_id)
                truck_obj.save()
                print("SAVING Truck Details")

                # Insurance Details

                insurance_doc = insurance_detail_form.cleaned_data['insurance_doc']
                ins_from_date = insurance_detail_form.cleaned_data['ins_from_date']
                ins_to_date = insurance_detail_form.cleaned_data['ins_to_date']

                print("Check...1")
                insurance_obj = InsuranceDetails(truck=getTruck(reg_id),insurance_doc=insurance_doc,ins_from_date=ins_from_date,ins_to_date=ins_to_date)
                print("Check...1--saving")
                # Fitness Details
                fitness_doc = fitness_detail_form.cleaned_data['fitness_doc']
                fit_from_date = fitness_detail_form.cleaned_data['fit_from_date']
                fit_to_date = fitness_detail_form.cleaned_data['fit_to_date']

                print("Check...2")
                fitness_obj = FitnessDetails(truck=getTruck(reg_id),fitness_doc=fitness_doc,fit_from_date=fit_from_date,fit_to_date=fit_to_date)
                print("Check...2--saving")
                # Pollution Certificate
                pollution_doc = pollution_certificate_form.cleaned_data['pollution_doc']
                pol_from_date = pollution_certificate_form.cleaned_data['pol_f_date']
                pol_to_date = pollution_certificate_form.cleaned_data['pol_to_date']

                print("Check...3")
                pol_obj = PollutionCertificate(truck=getTruck(reg_id),pollution_doc=pollution_doc,pol_from_date=pol_from_date,pol_to_date=pol_to_date)

                print("Check...3--saving")
                print("Trying to save all objs !!")
                pol_obj.save()
                insurance_obj.save()
                fitness_obj.save()

                return redirect('/')
            else:
                print("TRUCK Form encountered errors",truck_details_form.errors,insurance_detail_form.errors,fitness_detail_form.errors,pollution_certificate_form.errors)
    else:
        truck_details_form = TruckForm()
        insurance_detail_form = InsuranceDetailsForm()
        fitness_detail_form = FitnessDetailsForm()
        pollution_certificate_form = PollutionCertificateForm()
    return render(request,"manager/addtrucks.html",{'truck_details_form':truck_details_form,'insurance_detail_form':insurance_detail_form,'fitness_detail_form':fitness_detail_form,'pollution_certificate_form':pollution_certificate_form})


@login_required
def viewtruck(request,reg_id):
    truck_data = None
    list_of_drivers = None

    if request.method=='POST':
        pass
    else:
        truck_data = getTruckData(reg_id)
        list_of_transporters = getAvailableTransporters()

    return render(request,"manager/viewtruck.html",{'truck_data':truck_data,'list_of_transporters':list_of_transporters})

@login_required
def assign(request,reg_id,transporter_id):

    if request.method=='GET':
        assignTransporter(reg_id,transporter_id)
        return redirect('/')
    else:
        pass


@login_required
def addtransporter(request):
    transporter_details_form = TransporterDetailsForm()

    if request.method == 'POST':
        transporter_details_form = TransporterDetailsForm(request.POST)

        if transporter_details_form.is_valid():
            print("_________FORM IS VALID________")
            name = transporter_details_form.cleaned_data['transporter_name']
            email = transporter_details_form.cleaned_data['transporter_email']
            transporter_age = transporter_details_form.cleaned_data['transporter_age']
            unique_id = uuid.uuid4().hex[:6].upper()
            transporter_id ="".join(("TID",unique_id))
            user = User.objects.create_user(username=transporter_id,password=transporter_id,first_name=name,email=email)
            user.save()
            user_obj = User.objects.get(username=transporter_id)
            transporter_obj = TransporterDetails(user=user_obj,transporter_id=transporter_id,transporter_name=name,transporter_age=transporter_age,transporter_email=email)
            transporter_obj.save()
            return redirect('/')
        else:
            print(transporter_details_form.errors)
    else:
        return render(request,"manager/addtransporter.html",{'transporter_details_form':transporter_details_form})

@login_required
def alltransporters(request):
    all_transporters = None
    if request.method == 'GET':

        all_transporters = getAllTransporters()
    return render(request,"manager/alltransporters.html",{'all_transporters':all_transporters})




@login_required
def alltrucks(request):
    pass
