from django.contrib import admin
from . models import TransporterDetails,Reminder

# Register your models here.
admin.site.register(TransporterDetails)
admin.site.register(Reminder)
