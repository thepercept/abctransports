from django import forms
from django.contrib.auth.models import User


class LoginForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Transporter ID'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','placeholder':'Password'}))

    class Meta:
        model = User
        fields = ['username','password']


class TransporterDetailsForm(forms.Form):
    transporter_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Name'}))
    transporter_age = forms.CharField(widget=forms.TextInput(attrs={'type':'number','min':'18','max':'100','class':'form-control','placeholder':'Age'}))
    transporter_email = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','placeholder':'Email'}))
