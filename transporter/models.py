from django.db import models
from manager.models import Truck
from django.contrib.auth.models import User

from datetime import datetime, timedelta
from django.utils import timezone


# Create your models here.



class TransporterDetails(models.Model):
    AVAILABILITY_CHOICES = (
        ('yes','Available'),
        ('no','Not Available'),
    )

    truck = models.OneToOneField(Truck,null=True,blank=True)
    user = models.OneToOneField(User,null = False)
    transporter_id = models.CharField (max_length=9, blank=True, null=False,)
    transporter_name = models.CharField (max_length=40, blank=False, null=False)
    transporter_age = models.CharField (max_length=20, blank=False, null=False)
    transporter_email = models.EmailField(max_length=60, blank=False, null=False)
    available = models.CharField(choices=AVAILABILITY_CHOICES,max_length=3, null=False,default="yes")
    registration_date = models.DateTimeField(default=timezone.now)
    def __str__(self):
        return "(%s-%s) " % (self.transporter_name,self.transporter_id)


class Reminder(models.Model):

    STATUS_CHOICES = (
        ('NONE','--SELECT--'),
        ('active','Active'),
        ('sent','Sent'),
        ('read','Read'),
    )
    transporter = models.ForeignKey(TransporterDetails,null=False)
    reminder_date = models.DateField()
    reminder_note = models.CharField(max_length=100, null=False,default="NONE")
    reminder_status = models.CharField(choices=STATUS_CHOICES,max_length=10, null=False,default="NONE")

    def __str__(self):
        return "(%s-%s) " % (self.reminder_note,self.transporter.transporter_name)
