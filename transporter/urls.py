from django.conf.urls import url
from transporter.views import tp_login,tp_index,tp_mark_as_read

urlpatterns = [

    url(r'^login/$',tp_login,name='tp_login'),
    url(r'^$',tp_index,name='tp_index'),
    url(r'^mark_as_read/$',tp_mark_as_read,name='mark_as_read'),

]
