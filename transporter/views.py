from django.shortcuts import render
from transporter.forms import LoginForm
from transporter.models import TransporterDetails,Reminder
from django.contrib.auth import authenticate,login,logout
from abctransports import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from manager.utility import convertListToJson


# Create your views here.
def tp_login(request):
    print("Transporter Login Page")
    #next = request.GET.get('next','/transporter/')
    login_form = LoginForm()
    if request.method == 'POST':
        print("INSIDE POST..................................")
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username,password=password)

        if user is not None:
            print("USER PRESENT..................................",user.username)
            if user.is_active:
                print("USER ACTIVE..................................")

                transporter_instance = TransporterDetails.objects.get(transporter_id=user.username)


                login(request,user)
                request.session['transporter_id'] = transporter_instance.transporter_id
                print("Session storage ::",request.session,transporter_instance.transporter_id)
                return HttpResponseRedirect('/transporter/')
            else:
                HttpResponse("Inactive User")
        else :
            print("NO USER PRESENT..................................")
            return HttpResponseRedirect(settings.LOGIN_URL)
    return render(request, "transporter/login.html", {'login_form':login_form})

@login_required(redirect_field_name='/transporter/')
def tp_index(request):


    if 'transporter_id' in request.session:
        transporter_id = request.session['transporter_id']
        transporter_instance = TransporterDetails.objects.get(transporter_id=transporter_id)

        new_reminder_list = []
        read_reminder_list = []




        new_reminders = Reminder.objects.filter(transporter=transporter_instance,reminder_status="sent")
        read_reminders = Reminder.objects.filter(transporter=transporter_instance,reminder_status="read")

        new_rem_data = []
        read_rem_data = []

        if len(new_reminders) > 0:
            for reminder in new_reminders:


                new_rem_data.append(reminder.transporter.transporter_name)
                new_rem_data.append(reminder.reminder_note)
                new_rem_data.append(str(reminder.reminder_date))
                new_rem_data.append(reminder.reminder_status)
                new_reminder_list.append(new_rem_data)
                new_rem_data = []
        else:
            new_reminder_list = None

        if len(read_reminders) > 0:
            for reminder in read_reminders:

                read_rem_data.append(reminder.transporter.transporter_name)
                read_rem_data.append(reminder.reminder_note)
                read_rem_data.append(str(reminder.reminder_date))
                read_rem_data.append(reminder.reminder_status)
                read_reminder_list.append(read_rem_data)
                read_rem_data = []
        else:
            read_reminder_list = None


        new_reminder_json_array = convertListToJson(new_reminder_list)
        read_reminder_json_array = convertListToJson(read_reminder_list)

    else:
        print("Logout admin and login as a transporter")

    print(read_reminder_json_array,"____________________________________________",new_reminder_json_array)

    return render(request,"transporter/index.html",{'new_reminder_json_array':new_reminder_json_array,'read_reminder_json_array':read_reminder_json_array,'transporter_id':transporter_id})


@login_required
def tp_mark_as_read(request):
    if 'transporter_id' in request.session:
        transporter_id = request.session['transporter_id']
        transporter_instance = TransporterDetails.objects.get(transporter_id=transporter_id)

        new_reminders = Reminder.objects.filter(transporter=transporter_instance,reminder_status="sent")

        if len(new_reminders) > 0:
            for reminder in new_reminders:
                print(reminder.reminder_status,reminder.transporter)
                Reminder.objects.filter(transporter=transporter_instance).update(reminder_status="read")
                print("Updating")
            print("All notifications updated to READ")

        else:
            print("No new reminders")




    return HttpResponse("Updated !!")
